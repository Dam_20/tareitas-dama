import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AgregarproyectosComponent } from './agregarproyectos.component';

describe('AgregarproyectosComponent', () => {
  let component: AgregarproyectosComponent;
  let fixture: ComponentFixture<AgregarproyectosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AgregarproyectosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AgregarproyectosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
